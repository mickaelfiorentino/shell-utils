#!/bin/bash
################################################################################
# Bash utility functions
################################################################################

encrypt () {
    tar -cz ${1} | gpg -c -o ${1}.tgz.gpg
}
decrypt () {
    gpg -d ${1}.tgz.gpg | tar -xz
}
afind () {
    find $1 -name "*.$2"
}
efind () {
    find $1 -name "*.$2" -exec $3 {} \;
}
h265toh264 () {
    for f in *.mkv
    do
        audioformat=$(ffprobe -loglevel error -select_streams a:0 -show_entries stream=codec_name -of default=nw=1:nk=1 "$f")
        if [ "$audioformat" = "aac" ]; then
            ffmpeg -i "$f" -c:v libx264 -crf 23 -preset medium -c:a copy -movflags +faststart h264vids/"${f%.*}.mp4"
        else
            ffmpeg -i "$f" -c:v libx264 -crf 23 -preset medium -c:a aac -movflags +faststart h264vids/"${f%.*}.mp4"
        fi
    done
}
mkv2mp4 () {
    for i in *.mkv; do
        # HandBrakeCLI -Z "Universal" -i "$i" -o "${i/.mkv}.mp4"
        ffmpeg -y -i "$i" -map 0:v:0 -map 0:a:0 -map 0:s:0 -c:v libx264 -c:a copy -c:s mov_text -s hd720 "${i/.mkv}.mp4"
    done
}
flac2mp3 () {
    shopt -s globstar
    for i in **/*.flac; do
        ffmpeg -i "$i" -ab 320k -map_metadata 0 -id3v2_version 3 "${i/.flac}.mp3";
    done
}
download_mp3 () {
    local url="${1:?You must provide an URL}"
    local dir="${2:-.}"
    youtube-dl -v -x --add-metadata --yes-playlist --audio-format mp3 --audio-quality 0 -f bestaudio -o "${dir}/%(title)s.%(ext)s" "$url"
}
rename2utf8 () {
    local type=${1:-f}
    find . -type ${type} -print0 |
        while IFS= read -d '' f; do
            mv "${f}" "$(echo ${f} | iconv -c -t latin1//TRANSLIT | iconv -c -t utf8//TRANSLIT)"
        done
}
quick-rsync () {
    local from="${1}"
    local to="${2}"
    rsync -avzP --delete -W --inplace --ignore-existing ${from}/ ${to}/
}
