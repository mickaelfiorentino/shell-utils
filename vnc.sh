#!/bin/bash
################################################################################
# VNC for Remote Desktop through SSH
################################################################################
usage () {
    echo ""
    echo "$(basename $0)"
    echo ""
    echo "DESCRIPTION:"
    echo "    Utility that initiates a VNC connection through SSH"
    echo "    It relies on ssh config with appropriate forwarding of port 5901 to redirect VNC display."
    echo "    It uses ssh sockets to close the ssh tunnel when the VNC connection is terminated."
    echo ""
    echo "ARGUMENTS:"
    echo "    -h|--help              : Show the help"
    echo "    -r|--remote   <remote> : Name of the remote host to connect to using SSH (from ssh config)"
    echo "    -p|--vnc-port <port>   : Port number on which the VNC server is to be found. Default to 1."
    echo ""
    echo "EXAMPLE:"
    echo "    vnc.sh --remote=nas --vnc-port=2"
    echo ""
}

if [ $# -le 0 ]; then
    usage
    exit 0
fi

ARGS=$(getopt --options 'hr:p:' --longoptions 'help,remote:,vnc-port:' --name 'vnc.sh' -- "$@")

if [ $? -ne 0 ]; then
    echo "Something went wrong while parsing arguments..." >&2
    usage
    exit 1
fi

eval set -- "$ARGS"
unset ARGS

## Parsing arguments
while true; do
    case "$1" in
        '-h'|'--help')
            usage
            exit 0
            ;;
        '-r'|'--remote')
            case "$2" in
                "nas"|"gate")
                    remote="$2"
                    ;;
                *)
                    echo "Invalid argument '$2' for --remote, must be one of 'nas|gate'" >&2
                    usage
                    exit 1
                    ;;
            esac
            shift 2
            continue
            ;;
        '-p'|'--vnc-port')
            vncport="$2"
            shift 2
            continue
            ;;
        '--')
            shift
            break
            ;;
        *)
            echo 'Internal error!' >&2
            exit 1
            ;;
    esac
done

## Default values
vncport=${vncport:-1}

echo "Connecting to '$remote' remote using VNC port '$vncport'"

## Start SSH and waint until connexion is established
ssh -M -S ".socket_${remote}" -fnNT $remote

## Start VNC
vncviewer -FullScreenAllMonitors=0 -QualityLevel=9 -PasswordFile ~/.vnc/passwd localhost:$vncport

## Exit SSH when VNC stops
ssh -S ".socket_${remote}" -O exit $remote
