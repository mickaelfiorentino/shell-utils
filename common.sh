#!/bin/bash
################################################################################
# Common procedures
################################################################################
exit-if-argument-missing () {
    if [[ -z "${1+x}" ]]; then
        echo "Missing argument..."
        usage
        exit 1
    fi
}
exit-if-directory-does-not-exist () {
    if ! [[ -d "${1}" ]]; then
        echo "The directory ${1} does not exist..."
        usage
        exit 1
    fi
}
exit-if-directory-is-empty () {
    if [[ -n "$(find ${1} -maxdepth 0 -empty)" ]]; then
        echo "The directory ${1} is empty..."
        usage
        exit 1
    fi
}
init-file () {
    mkdir -p "$(dirname ${1})"
    true > "${1}"
}
