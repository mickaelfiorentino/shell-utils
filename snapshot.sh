#!/bin/bash
################################################################################
# Incremental snapshot
################################################################################
source "$(dirname $(realpath $0))/common.sh"

usage () {
    echo ""
    echo "$(basename $0)"
    echo ""
    echo "DESCRIPTION:"
    echo "    Utility that performs incremental snapshots on BTRFS"
    echo ""
    echo "ARGUMENTS:"
    echo "    -h|--help                    : Show the help"
    echo "    -s|--source  <source-subvol> : Path of the source subvolume"
    echo "    -d|--dest    <dest-subvol>   : Path of the destination snapshot subvolume"
    echo "    -t|--target  <target subdir> : (optional) Subdirectory under which snapshot will be saved; ie. DEST/TARGET/"
    echo ""
    echo "EXAMPLES:"
    echo "    # snapshot.sh --source=/mnt/data/shares --dest=/mnt/data/snapshots --target=shares"
    echo "    will make a snapshot of /mnt/data/shares under /mnt/data/snapshots/shares/$(date +%y-%m-%d_%H-%M)"
}

if [[ $# -le 0 ]]; then
    usage
    exit 1
fi

ARGS=$(getopt --options 'hs:d:t:' --longoptions 'help,source:,dest:,target:' --name "$(basename $0)" -- "$@")

if [[ $? != 0 ]]; then
    echo "Something went wrong while parsing arguments..." >&2
    usage
    exit 1
fi

eval set -- "$ARGS"
unset ARGS

## Parsing arguments
while true; do
    case "$1" in
        '-h'|'--help')
            usage
            exit 0
            ;;
        '-s'|'--source')
            eval srcdir="$2"
            shift 2
            continue
            ;;
        '-d'|'--dest')
            eval dstdir="$2"
            shift 2
            continue
            ;;
        '-t'|'--target')
            target="$2"
            shift 2
            continue
            ;;
        '--')
            shift
            break
            ;;
        *)
            echo 'Internal error!' >&2
            exit 1
            ;;
    esac
done

## Check arguments
exit-if-argument-missing ${srcdir}
exit-if-argument-missing ${dstdir}
exit-if-directory-does-not-exist ${srcdir}
exit-if-directory-is-empty ${srcdir}
exit-if-directory-does-not-exist ${dstdir}

## btrfs needs sudo... to avoid password, edit sudoers with visudo
# <user> ALL = NOPASSWD: /usr/bin/btrfs
btrfs="sudo btrfs"

# Checks whether source and destination directories are BTRFS subvolumes
# BTRFS subvolumes happen to have inode=256...
if ! [[ $(ls -di ${srcdir} | cut -d ' ' -f1) == 256 ]]; then
    echo "Source directory ${srcdir} is not a BTRFS subvolume... "
    exit 1
fi
if ! [[ $(ls -di ${dstdir} | cut -d ' ' -f1) == 256 ]]; then
    echo "Destination snapshot directory ${dstdir} is not a BTRFS subvolume... "
    exit 1
fi

snapshot="${dstdir}/${target:+${target}/}$(date +%y-%m-%d_%H-%M)"

## Make snapshot directory in case it does not exist
snapshotdir=$(dirname ${snapshot})
mkdir -p ${snapshotdir}

## Make snapshot only if a file was modified on the source since the last snapshot
makesnapshot=true
if [[ -z "$(find ${snapshotdir} -maxdepth 0 -empty)" ]]; then
    lastsnapshot=$(${btrfs} subvolume list -s ${srcdir} | sort -rn | head --lines=1)
    lastsnapshotpath=$(echo "${lastsnapshot}" | grep -Po 'path[[:space:]]\K([[:print:]])+')
    lastsnapshotgen=$(echo "${lastsnapshot}" | grep -Po '[[:space:]]gen[[:space:]]\K([[:digit:]])+')
    srcgen=$(${btrfs} subvolume find-new ${srcdir} ${lastsnapshotgen} | grep -Po '^transid([[:space:]]?[[:alpha:]]?)+\K([[:digit:]]+)')
    if [[ ("$(basename ${lastsnapshotpath})" == "$(basename ${snapshot})") || (${lastsnapshotgen} == ${srcgen}) ]]; then
        makesnapshot=false
    fi
fi
if [[ "$makesnapshot" == "true" ]]; then
    ${btrfs} subvolume snapshot -r ${srcdir} ${snapshot}
fi
