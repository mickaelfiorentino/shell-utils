#!/bin/bash
################################################################################
# Backup
################################################################################
source "$(dirname $(realpath $0))/common.sh"

usage () {
    echo ""
    echo "$(basename $0)"
    echo ""
    echo "DESCRIPTION:"
    echo "    Utility that performs backup of SOURCE to DEST with RSYNC"
    echo ""
    echo "ARGUMENTS:"
    echo "    -h|--help                : Show the help"
    echo "    -s|--source <source-dir> : Path of the source directory"
    echo "    -d|--dest   <dest-dir>   : Path of the destination directory"
    echo "    -l|--log    <log-file>   : (optional) Path of the log file"
    echo "    -c|--chown  <USER:GROUP> : (optional) chown argument directly passed to rsync"
    echo "    -n|--dry-run             : (optional) Dry run flag passed to rsync"
    echo ""
    echo "EXAMPLES:"
    echo "    # backup.sh --source=~/Documents --dest=nas:/mnt/data/shares/mickael"
    echo "    will synchronize the content of the Documents folder within the /mnt/data/shares/mickael/Documents folder on the nas remote (through ssh)"
    echo ""
}

if [[ $# -le 0 ]]; then
    usage
    exit 1
fi

ARGS=$(getopt --options 'hs:d:l:c:n' --longoptions 'help,source:,dest:,log:,chown:,dry-run' --name "$(basename $0)" -- "$@")

if [[ $? != 0 ]]; then
    echo "Something went wrong while parsing arguments..." >&2
    usage
    exit 1
fi

eval set -- "$ARGS"
unset ARGS

## Parsing arguments
while true; do
    case "$1" in
        '-h'|'--help')
            usage
            exit 0
            ;;
        '-s'|'--source')
            eval srcdir="$2"
            shift 2
            continue
            ;;
        '-d'|'--dest')
            eval dstdir="$2"
            shift 2
            continue
            ;;
        '-l'|'--log')
            eval log="$2"
            shift 2
            continue
            ;;
        '-c'|'--chown')
            chown="$2"
            shift 2
            continue
            ;;
        '-n'|'--dry-run')
            dryrun="--dry-run"
            shift
            continue
            ;;
        '--')
            shift
            break
            ;;
        *)
            echo 'Internal error!' >&2
            exit 1
            ;;
    esac
done

## Check arguments
exit-if-argument-missing ${srcdir}
exit-if-argument-missing ${dstdir}
exit-if-directory-does-not-exist ${srcdir}
exit-if-directory-is-empty ${srcdir}

## Defaults
timeout=${timeout:-600}
src=$(basename ${srcdir})
backup=${dstdir}/${src}
log=${log:-~/.local/backups/logs/${src}_$(date +%y-%m-%d_%H-%M).log}
rsync_opt="--cvs-exclude --delete --delete-excluded --chown=${chown} --timeout=${timeout}"

## Make backup
filestotransfer=$(rsync -an ${rsync_opt} --stats ${srcdir}/ ${backup} | grep -Po 'Number of regular files transferred: \K([[:digit:]]+)')
if [[ ${filestotransfer} > 0 ]]; then
    init-file "${log}"
    rsync -aPhv ${rsync_opt} ${dryrun} --log-file="${log}" ${srcdir}/ ${backup}
fi
